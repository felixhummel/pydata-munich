FROM python:3.6-stretch

LABEL de.felixhummel.project=pydata-munich

ENV BASE_DIR=/opt/project
ENV DATA_DIR=/srv/django

# install dependencies
RUN pip install -U pip==10.0.1 setuptools==39.0.1

RUN apt-get update && apt-get --yes install \
        bash \
        bash-completion \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir uWSGI==2.0.17

RUN useradd --uid 3000 django

COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# prepare directories
COPY . $BASE_DIR
RUN mkdir $DATA_DIR
ENV STATIC_ROOT=$DATA_DIR/static
RUN mkdir $STATIC_ROOT

# make sure django owns everything
RUN chown -R django:django $BASE_DIR $DATA_DIR

WORKDIR $BASE_DIR

# project-scope actions
RUN ./bin/uid_hack.sh

USER django

RUN ./manage.py collectstatic
ENV UWSGI_STATIC_MAP=/static=$STATIC_ROOT

EXPOSE 8000

VOLUME ["/srv/django"]

CMD ["uwsgi", "uwsgi.ini"]
