from django.http import HttpResponse

from app.models import Greeting


def index(request):
    try:
        greeting = Greeting.objects.latest().value
    except Greeting.DoesNotExist:
        greeting = 'Hello World!'
    return HttpResponse(f'{greeting}\n', content_type='text/plain')
