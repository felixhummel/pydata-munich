from django.test import TestCase

from app.models import Greeting


class ExampleTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        Greeting.create(value='Hallo Welt!')

    def test_greeting(self):
        response = self.client.get('/')
        self.assertEqual(response['content-type'], 'text/plain')
        expected = 'Hallo Welt!\n'
        actual = response.content.decode('utf-8')
        self.assertEqual(actual, expected)
