from django.db import models


class Greeting(models.Model):
    value = models.TextField()
    dt = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        get_latest_by = 'dt'

    def __str__(self):
        return f'Greeting "{self.value}"'

    @classmethod
    def create(cls, value):
        self = cls(value=value)
        self.save()
        return self
