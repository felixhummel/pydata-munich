# Getting Started

Set hostnames in `/etc/hosts`:
```
127.0.0.1   pydata-munich.example.org
```

Check your tools:
```
docker-compose --version
# docker-compose version 1.19.0-rc1, build d149ccd
docker --version
# Docker version 17.12.0-ce, build c97c6d6
```

Initialize Development Environment
```
./bin/dev_setup.sh
```

# Clear Development Environment - WARNING! This is destructive!
```
./bin/DANGER_clean_dev_env.sh
```
