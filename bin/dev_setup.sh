#!/bin/bash
set -euo pipefail

log() { echo "$@" 2>&1; }
warn() { log "WARNING $@"; }

[[ ! -f .env ]] && ./bin/generate_env_file.sh > .env || warn ".env exists. skipping"

# Link `docker-compose.override.yml` to base dir:
[[ -f docker-compose.override.yml ]] || ln -s env/dev/docker-compose.override.yml

# Build container
docker-compose build

docker-compose up -d postgres
docker-compose run --rm app ./bin/_wait ./manage.py is_db_up

docker-compose run --rm app ./manage.py migrate
docker-compose run --rm app ./manage.py loaddata admin
docker-compose up -d

cat <<'EOF'


http://pydata-munich.example.org
docker-compose logs -f
EOF
