#!/bin/sh
set -euo pipefail

cat <<EOF
COMPOSE_UID=$(id -u)
COMPOSE_GID=$(id -g)

IMG=registry.felixhummel.de/felix/pydata-munich
PROJECT_LABEL=de.felixhummel.project=pydata-munich
EOF
