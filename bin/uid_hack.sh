#!/bin/sh
# add usernames for UIDs called "u$UID", e.g. 1001 --> "u1001"
# 1000 to 2000 should suffice for now

id_exists() {
  getent passwd $1 > /dev/null
}

for i in $(seq 1000 2000); do
  if id_exists $i; then
    echo "exists: $i"
    continue
  fi
  echo "u${i}:x:${i}:${i}::/nonexistent:/bin/false" >> /etc/passwd
done
