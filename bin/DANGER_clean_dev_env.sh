#!/bin/bash
set -euo pipefail

source .env

[[ -n $PROJECT_LABEL ]]
docker-compose down --remove-orphans
volumes=$(docker volume ls -q --filter label=$PROJECT_LABEL)
[[ -n $volumes ]] && docker volume rm $volumes
